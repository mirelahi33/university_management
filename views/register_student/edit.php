<?php
include "../../vendor/autoload.php";
use App\Student\Student;
$obj=new Student();
$onedata = $obj->showData($_GET['id']);

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">University Management System</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>NM Nahid <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="../../index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="javascript:" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Department <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="#">Save Department</a>
                        </li>
                        <li>
                            <a href="#">View All Department</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-arrows-v"></i>Courses<i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo1" class="collapse">
                        <li>
                            <a href="../../views/courses/save_course.php">Save Course </a>
                        </li>
                        <li>
                            <a href="../../views/courses/show_all_course.php">Show All Course </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i>Teachers<i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo2" class="collapse">
                        <li>
                            <a href="../../views/Teachers/save_teachers.php">Save Teachers</a>
                        </li>
                        <li>
                            <a href="../../views/Teachers/show_all_teachers.php">Show All Teachers </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i>Course Assign Teacher<i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo3" class="collapse">
                        <li>
                            <a href="../../views/course_assign_to_teacher/course_assign_to_teacher.php">Course Assign to Teachers </a>
                        </li>
                        <li>
                            <a href="../../views/course_assign_to_teacher/show_assign_to_teacher.php">Show Assign to Teachers </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../../views/view_course_statics/viewcourse_statics.php"><i class="fa fa-fw fa-table"></i> Course Statics</a>
                </li>

                <li>
                    <a href="javascript:" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-arrows-v"></i>Register Student<i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo4" class="collapse">
                        <li>
                            <a href="../../views/register_student/register_student.php">Register Student </a>
                        </li>
                        <li>
                            <a href="../../views/register_student/show_register_student.php">Show Register Student</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> <a href="show_register_student.php">Show Register Students</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">Register Student</label>
                        </div>

                        <form method="post" action="update.php" name="register_student">

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" value="<?php echo $onedata['name'];?>" id="name" placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" value="<?php echo $onedata['email'];?>" id="email" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" name="date" value="<?php echo $onedata['createdAt'];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label txt">Address</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="address" id="exampleTextarea" rows="3"><?php echo $onedata['address'];?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Department</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="exampleSelect1"  name="department">
                                            <option><?php echo $onedata['department'];?></option>
                                    </select>
                                    <input type="hidden" name="id"  value="<?php echo $onedata['id']?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" value="update" class="btn btn-default saveBtn"  id="">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div><br><br><br><br>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>
<script src="../../js/form_validation4.js"></script>
<script src="../../js/jquery.validate.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 60%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .table{
        margin: auto;
        width: 80%;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt{
        font-size: 16px;
    }
    .saveBtn{
        float: right;
    }
    .msg{
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>