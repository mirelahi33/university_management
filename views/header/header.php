<script type="text/javascript" src="../../js/jquery.min.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../../index.php">University Management System</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>NM Nahid <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="../../index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Department <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="../../views/department/save_department.php">Save Department</a>
                    </li>
                    <li>
                        <a href="../../views/department/view_all_department.php">View All Department</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-arrows-v"></i>Courses<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo1" class="collapse">
                    <li>
                        <a href="../../views/courses/save_course.php">Save Course </a>
                    </li>
                    <li>
                        <a href="../../views/courses/show_all_course.php">Show All Course </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i>Teachers<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo2" class="collapse">
                    <li>
                        <a href="../../views/Teachers/save_teachers.php">Save Teachers</a>
                    </li>
                    <li>
                        <a href="../../views/Teachers/show_all_teachers.php">Show All Teachers </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i>Course Assign Teacher<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo3" class="collapse">
                    <li>
                        <a href="../../views/course_assign_to_teacher/course_assign_to_teacher.php">Course Assign to Teachers </a>
                    </li>
                    <li>
                        <a href="../../views/course_assign_to_teacher/show_assign_to_teacher.php">Show Assign to Teachers </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="../../views/view_course_statics/viewcourse_statics.php"><i class="fa fa-fw fa-table"></i> Course Statics</a>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-arrows-v"></i>Register Student<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo4" class="collapse">
                    <li>
                        <a href="../../views/register_student/register_student.php">Register Student </a>
                    </li>
                    <li>
                        <a href="../../views/register_student/show_register_student.php">Show Register Student</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-arrows-v"></i>Allocate Classroom<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo5" class="collapse">
                    <li>
                        <a href="../../views/allocate_classroom/allocate_classroom.php">Allocate Classroom</a>
                    </li>
                    <li>
                        <a href="../../views/allocate_classroom/show_allocate_classroom.php">Show Allocate Classroom</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="../../views/class_schedule_room_info/schedule_room_info.php"><i class="fa fa-fw fa-table"></i>Class Schedule & Room Info</a>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo6"><i class="fa fa-fw fa-arrows-v"></i>Enroll Course<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo6" class="collapse">
                    <li>
                        <a href="../../views/enroll_course/enroll_course.php">Enroll In a Course</a>
                    </li>
                    <li>
                        <a href="../../views/enroll_course/show_enroll_course.php">Show Enroll In a Course</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:" data-toggle="collapse" data-target="#demo7"><i class="fa fa-fw fa-arrows-v"></i>Student Result<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo7" class="collapse">
                    <li>
                        <a href="../../views/save_student_result/save_student_result.php">Save Student Result</a>
                    </li>
                    <li>
                        <a href="../../views/save_student_result/show_student_result.php">Show Student Result</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="../../views/view_result/view_result.php"><i class="fa fa-fw fa-table"></i>View Result</a>
            </li>

            <li>
                <a href="../../views/Unassign_all_course/unassign_all_course.php"><i class="fa fa-fw fa-table"></i>Unassign All Course</a>
            </li>


            <li>
                <a href="../../views/unallocate_all_classrooms/unallocate_all_classrooms.php"><i class="fa fa-fw fa-table"></i>Unallocate All Classrooms</a>
            </li>


        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
