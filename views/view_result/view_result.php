<?php
include "../../vendor/autoload.php";
use App\Enroll_Course\Enroll_Course;

$obj = new Enroll_Course();
$data = $obj->getenrollcourse();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <?php include_once "../header/header.php"?>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#std_result").change(function () {
                var std_result = $(this).val();

                console.log(std_result);

                $.ajax
                ({
                    url: 'ajaxdata.php',
                    type: 'POST',
                    data: {student_result: std_result},
                    dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        var std_name = response['name'];
                        // console.log(std_name);
                        var std_email = response["email"];

                        var std_department = response["department"];

//                        var len = response['courses'].length;
//                        console.log(len);
//                        $("#select_course").empty();
//
//                        $("#select_course").append("<option value=''> " + 'Select Course' + " </option>");
//
//                        for (var i = 0; i < len; i++) {
//                            var id =response['courses'][i]['id'];
//                            var s_course = response['courses'][i]['select_course'];
//                            $("#select_course").append("<option value='" + id + "'> " + s_course + " </option>");
//                        }

                        $("#s_name").val(std_name);
                        $("#s_email").val(std_email);
                        $("#s_department").val(std_department);

                    }
                });
            });

        });
    </script>

        <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">View Result</label>
                        </div>
                        <form method="post" action="#">

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Student Reg. No</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="std_result" name="student_reg_no">
                                        <option>Student Reg. No</option>
                                        <?php foreach ($data as $any){
                                        ?>
                                        <option><?php echo $any['student_reg_no']; ?></option>
                                        <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="s_name" placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" id="s_email" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Department</label>
                                <div class="col-sm-8">
                                    <input type="text" name="department" class="form-control" id="s_department" placeholder="Department">
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" value="Make PDF" class="btn btn-default saveBtn"  id="">
                                </div>
                            </div>
                        </form>
                        <br><br>
                    </div>

                    <div class="row table">
                        <div class="col-md-12">
                            <form method="post" action="#">
                                <div class="form-group row">
                                    <table class="table">
                                        <thead class="thead-default">
                                        <tr>
                                            <th>Course Code</th>
                                            <th>Name</th>
                                            <th>Grade</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>153</td>
                                            <td>NM Nahid</td>
                                            <td>A+</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </form>

                        </div>
                </div>
            </div><br><br><br><br>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 60%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .table{
        margin: auto;
        width: 90%;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt{
        font-size: 16px;
    }
    .saveBtn{
        float: right;
    }
    .msg{
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>