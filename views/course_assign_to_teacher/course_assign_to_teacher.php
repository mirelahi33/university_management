<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../js/jquery.js"></script>
  <!--  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <?php include_once "../header/header.php";

    include "../../vendor/autoload.php";
    use App\save_department\Save_department;

    //session_start();
    $obj = new Save_department();
    $data = $obj->getdepartment();
    ?>


    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Forms
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="../../index.php">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> <a href="show_assign_to_teacher.php">Show Assign To Teachers</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">Course Assign to Teacher</label>
                        </div>
                        <form method="post" action="store.php">

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Department</label>
                                <div class="col-sm-8">

                                    <select class="form-control" name="department_name" id="dep_id">
                                    <option> Select Department</option>
                                    <?php foreach ($data as $any){ ?>
                                        <option value="<?php echo $any['name']; ?>"><?php echo $any['name']; ?> </option>
                                    <?php }; ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Teacher</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="teacher" name="teacher_name">
                                        <option>Select Teacher</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Credit to be taken</label>
                                <div class="col-sm-8">
                                    <input type="text" name="credit_taken" class="form-control" id="credit_taken"
                                           placeholder="credit to be taken">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Remaining credit</label>
                                <div class="col-sm-8">
                                    <input type="text" name="remaining_credit" class="form-control" id="remaining_credit"
                                           placeholder="Remaining credit">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Course Code</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="course_code" name="course_code">
                                        <option>1002</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Credit</label>
                                <div class="col-sm-8">
                                    <input type="text" name="credit" class="form-control" id="credit" placeholder="Credit">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" value="Assign" class="btn btn-default saveBtn" id="">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <br><br><br><br>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        $("#dep_id").change(function () {
            var depp_id = $(this).val();
        //    console.log(depp_id);
            $.ajax
             ({
                url: 'ajaxdata.php',
                type: 'post',
                data: {depp_id: depp_id},
                dataType: 'json',

                success: function (response) {
                    // console.log(depp_id);
                    console.log(response['teachers']);
                    var len = response['teachers'].length;
                    $("#teacher").empty();
                    $("#teacher").append("<option value=''> " + 'Select Teacher' + " </option>");

                    for (var i = 0; i < len; i++) {
                        var id = response['teachers'][i]['id'];
                        var tea_name = response['teachers'][i]['name'];

                        $("#teacher").append("<option value='" + id + "'> " + tea_name + " </option>");
                    }

                    $("#course_code").empty();
                    $("#course_code").append("<option value=''> " + 'Select course code' + " </option>");
                    var clen = response['courses'].length;
                    for (var i = 0; i < clen; i++) {
                        var id = response['courses'][i]['id'];
                        var code = response['courses'][i]['code'];
                       // var name = response['courses'][i]['name'];
                       // var credit = response['courses'][i]['credit'];

                        $("#course_code").append("<option value='" + id + "'> " + code + " </option>");
                      //  $("#name").val(name);
                       // $("#credit").val(credit);
                    }
                }

            });
        });

        $("#teacher").change(function () {
            var teach_id = $(this).val();
            //    console.log(depp_id);
            $.ajax
            ({
                url: 'ajaxdata.php',
                type: 'post',
                data: {teach_id: teach_id},
                dataType: 'json',

                success: function (response) {
                    // console.log(depp_id);
                  var cc= (response['teacher']['credit']);
                  var ac= (response['a_c']['assign_credit']);
                    console.log(ac);
                   //var len = response.length;
                   //var crd= response['credit']
                   // $("#teacher").empty();
                    $("#credit_taken").val(cc);
                    $("#remaining_credit").val(cc-ac);



                }

            });
        });




        $("#course_code").change(function () {
            var c_id = $(this).val();
               //console.log(c_id);
            $.ajax
            ({
                url: 'ajaxdata.php',
                type: 'post',
                data: {c_id: c_id},
                dataType: 'json',

                success: function (response) {
                     console.log(response);
                   var cc= (response['name']);
                    var ac= (response['credit']);
                  // console.log(ac);

                    $("#name").val(cc);
                    $("#credit").val(ac);

                }

            });
        });


    });


</script>



<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable {
        width: 70%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }

    .table {
        margin: auto;
        width: 80%;
        border-radius: 4px;
    }

    .title {
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }

    .txt {
        font-size: 16px;
    }

    .saveBtn {
        float: right;
    }

    .msg {
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>