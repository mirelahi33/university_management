<?php
include "../../vendor/autoload.php";
use App\Teachers\Teachers;

$obj=new Teachers();
$data = $obj->getteachers();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <?php include_once "../header/header.php"?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> <a href="save_teachers.php">Save Teachers</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">Show All Course</label>
                        </div>
                        <form method="post" action="#">
                            <div class="form-group row">
                                <table class="table">
                                    <thead class="thead-default">
                                    <tr>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Email</th>
                                        <th>Contact No.</th>
                                        <th>Designation</th>
                                        <th>Department</th>
                                        <th>Credit to be taken</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data as $any) {
                                        ?>
                                        <tr>
                                            <td><?php echo $any['name'];?></td>
                                            <td><?php echo $any['address'];?></td>
                                            <td><?php echo $any['email'];?></td>
                                            <td><?php echo $any['contact'];?></td>
                                            <td><?php echo $any['designations'];?></td>
                                            <td><?php echo $any['department'];?></td>
                                            <td><?php echo $any['credit'];?></td>
                                            <td>
                                                <a href="edit.php?id=<?php echo $any['id']; ?>">
                                                    <li class="fa fa-edit size"></li>
                                                    <a/>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <a href="delete.php?id=<?php echo $any['id']; ?>">
                                                        <li class="glyphicon glyphicon-trash"></li>
                                                    </a>
                                            </td>

                                        </tr>
                                        <?php
                                       }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>

                    </div>
                </div>
            </div><br><br><br><br>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 90%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .table{
        margin: auto;
        width: auto;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt{
        font-size: 16px;
    }
    .saveBtn{
        float: right;
    }
    .msg{
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>