<?php
include "../../vendor/autoload.php";
use App\save_department\Save_department;

//session_start();
$obj=new Save_department();
$data= $obj->getdepartment();
$data2=$obj->getdesignation();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link href="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <?php include_once "../header/header.php"?>

        <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> <a href="show_all_teachers.php">Show All Teachers</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">Save Teacters</label>
                        </div>
                        <form method="post" action="teacher_store.php" name="save_teacher">
                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="" placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label txt">Address</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="address" id="exampleTextarea" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" id="" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Contact No.</label>
                                <div class="col-sm-8">
                                    <input type="text" name="contact" class="form-control" id="" placeholder="Contact No.">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Designation</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="exampleSelect1"  name="designation">

                                        <?php foreach ($data2 as $any){
                                            ?>

                                            <option><?php echo $any['designation']; ?> </option>
                                        <?php }; ?>


                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Department</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="department" name="department">
                                        <?php foreach ($data as $any){
                                            ?>

                                            <option><?php echo $any['name']; ?> </option>
                                        <?php }; ?>


                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="code" class="col-sm-4 col-form-label txt">Credit to be taken</label>
                                <div class="col-sm-8">
                                    <input type="text" name="credit_to_be_taken" class="form-control" id="" placeholder="">
                                </div>
                            </div>

                              <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" value="Save" class="btn btn-default saveBtn"  id="">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div><br><br><br><br>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>
<script src="../../js/form_validation3.js"></script>
<script src="../../js/jquery.validate.min.js"></script>


<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 60%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .table{
        margin: auto;
        width: 80%;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt{
        font-size: 16px;
    }
    .saveBtn{
        float: right;
    }
    .msg{
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>