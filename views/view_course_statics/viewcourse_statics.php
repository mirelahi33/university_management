<?php include_once "../header/header.php";

include "../../vendor/autoload.php";
use App\save_department\Save_department;

//session_start();
$obj = new Save_department();
$data = $obj->getdepartment();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <?php include_once "../header/header.php"?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row tableSt">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p class="title">View Course Statics</p>
                        </div>

                        <div class="col-md-12">
                            <form action="#" method="">
                                <div class="department">
                                    <label for="credit" class="col-sm-4 col-form-label txt">Department</label>
                                    <div class="col-sm-6">
                                        <select name="dep_id" id="dep_id" class="form-control">
                                            <option> Select Department</option>
                                            <?php foreach ($data as $any){ ?>
                                                <option value="<?php echo $any['name']; ?>"><?php echo $any['name']; ?> </option>
                                            <?php }; ?>
                                        </select>


                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-12">
                                <p class="title"> Course Information</p>
                            </div>

                            <br/><br/>
                            <div class="tableee">
                                <table class="table">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th>Code</th>
                                        <th>Name/Title</th>
                                        <th>Semester</th>
                                        <th>Assigned To</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td id="c_code">    </td>
                                        <td id="c_name">    </td>
                                        <td id="semester">    </td>
                                        <td id="c_teacher">    </td>

                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div><br/><br/><br/><br/>

            </div>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>


<script type="text/javascript">
    $(document).ready(function () {

        $("#dep_id").change(function () {
            var teach_id = $(this).val();
              // console.log(teach_id);
            $.ajax
            ({
                url: 'ajaxdata.php',
                type: 'post',
                data: {teach_id: teach_id},
                dataType: 'json',

                success: function (response) {
                    //console.log(response['data2']);
                  // console.log(response['code']);
                   var len = response['data1'].length;
                     $("#c_code").empty();
//
                    for (var i = 0; i < len; i++) {
                        var id = response['data1'][i]['id'];
                        var c_code = response['data1'][i]['code'];

                        $("#c_code").append("<option value='" + id + "'> " + c_code + " </option>");


                    }
                    $("#c_name").empty();
                    for (var i = 0; i < len; i++) {
                        var id = response['data1'][i]['id'];
                        var c_name = response['data1'][i]['name'];

                        $("#c_name").append("<option value='" + id + "'> " + c_name + " </option>");
                    }
                    $("#semester").empty();
                    for (var i = 0; i < len; i++) {
                        var id = response['data1'][i]['id'];
                        var semester = response['data1'][i]['semester'];

                        $("#semester").append("<option value='" + id + "'> " + semester + " </option>");
                    }


                    $("#c_teacher").empty();
                    for (var i = 0; i < len; i++) {
                        var id = response['data2'][i]['id'];
                        var tea_name = response['data2'][i]['name'];

                        $("#c_teacher").append("<option value='" + id + "'> " + tea_name + " </option>");
                    }

                }

            });
        });

    });



</script>


<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 90%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .tableSt{
        margin: auto;
        width: auto;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt {
        font-size: 16px;
    }
    .department{
        margin: auto;
        width:100%;
        text-align: center;
    }
    .tableee{
        border: 0px solid #000;
        width: 100%;

    }
</style>