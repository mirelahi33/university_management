<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <?php include_once "../header/header.php"?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">

                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row tableSt">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p class="title">View Class Schedule and Room Allocation Information</p>
                        </div>

                        <div class="col-md-12">
                            <form action="#" method="">
                                <div class="department">
                                    <label for="credit" class="col-sm-4 col-form-label txt">Department</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="exampleSelect1" name="department">
                                            <option>CSE</option>

                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-12">
                            <br/><br/>
                            <div class="tableee">
                                <table class="table">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th>Course Code</th>
                                        <th>Name</th>
                                        <th>Schedule Info</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Math-2323</th>
                                        <td>Computer Programming</td>
                                        <td>R. No :A-202,Tue,9:00 AM -12:00 PM;</td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div><br/><br/><br/><br/>

            </div>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 90%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .tableSt{
        margin: auto;
        width: auto;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt {
        font-size: 16px;
    }
    .department{
        margin: auto;
        width:100%;
        text-align: center;
    }
    .tableee{
        border: 0px solid #000;
        width: 100%;

    }
</style>