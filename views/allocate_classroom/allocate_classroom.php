<?php include_once "../header/header.php";

include "../../vendor/autoload.php";
use App\save_department\Save_department;

//session_start();
$obj = new Save_department();
$data = $obj->getdepartment();
?>

<?php include_once "../header/header.php";

include "../../vendor/autoload.php";
use App\courses\Save_courses;


//session_start();
$obj1 = new Save_courses();
$data1 = $obj1->getcourse();
$data2= $obj1->getrooms();
$data3=$obj1->getdays();

?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin </title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

   <?php include_once "../header/header.php"?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="../../index.php">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> <a href="show_allocate_classroom.php">Show Allocate Classrooms</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">Allocate Classrooms</label>
                        </div>
                        <form method="post" action="store.php">

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Department</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="department_name" id="department_name">
                                        <option> Select Department</option>
                                        <?php foreach ($data as $any){ ?>
                                            <option value="<?php echo $any['name']; ?>"><?php echo $any['name']; ?> </option>
                                        <?php }; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Course</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="course_name" id="course_name">
                                        <option> Select course</option>
                                        <?php foreach ($data1 as $any){ ?>
                                            <option value="<?php echo $any['name']; ?>"><?php echo $any['name']; ?> </option>
                                        <?php }; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Room No.</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="room_no" id="room_no">
                                        <option> Select rooms</option>
                                        <?php foreach ($data2 as $any){ ?>
                                            <option value="<?php echo $any['room_no']; ?>"><?php echo $any['room_no']; ?> </option>
                                        <?php }; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="credit" class="col-sm-4 col-form-label txt">Day</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="day" id="day">
                                        <option> Select Day</option>
                                        <?php foreach ($data3 as $any){ ?>
                                            <option value="<?php echo $any['day']; ?>"><?php echo $any['day']; ?> </option>
                                        <?php }; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="credit" class="col-md-4 col-form-label txt">From</label>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <input class="form-control"  type="time" name="time1"  size="6">
                                    </div>
                                    <div class="col-md-6">
                                       <!-- <input type="radio" name="am"> AM
                                        <input type="radio" name="pm"> PM-->
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit" class="col-md-4 col-form-label txt">To</label>
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <input class="form-control" type="time" name="time2" size="6">
                                    </div>
                                    <div class="col-md-6">
                                      <!--  <input type="radio" name="am"> AM
                                        <input type="radio" name="pm"> PM-->
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" value="Allocate" class="btn btn-default saveBtn"  id="">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <br><br><br><br>
            </div><br><br><br><br>

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>


<style>
    .mainTable{
        width: 60%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .table{
        margin: auto;
        width: 80%;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt{
        font-size: 16px;
    }
    .saveBtn{
        float: right;
    }
    .msg{
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>