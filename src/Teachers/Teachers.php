<?php

/**
 * Created by PhpStorm.
 * User: Ayon
 * Date: 17-Apr-17
 * Time: 2:25 PM
 */
namespace  App\Teachers;
use PDO;
//session_start();
class Teachers
{   private $id;
    private $address;
    private $name;
    private $email;
    private $contact;
    private $designations;
    private $credit;
    private $department;
    private $dbuser = 'root';
    private $dbpass = '';



    public function setData($data = '')
    {
        if(!empty($data['name'])){
            $this->name= $data['name'];
        }
        if(!empty($data['address'])) {
            $this->address = $data['address'];
        }
        if(!empty($data['contact'])) {
            $this->contact = $data['contact'];
        }
        if(!empty($data['designation'])) {
            $this->designations = $data['designation'];
        }
        if(!empty($data['department'])) {
            $this->department = $data['department'];
        }
        if(!empty($data['credit_to_be_taken'])) {
            $this->credit = $data['credit_to_be_taken'];
        }
        if(!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if(!empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;

    }


    public function showData($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "SELECT * FROM teachers WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public function upDate(){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "UPDATE teachers SET name=:i,address=:u,designations=:c,email=:d,department=:de,contact=:cnt,credit=:cre WHERE id=:id";

            $stmt = $pdo->prepare($query);

            $data = array(
                ':i' => $this->name,
                ':u' => $this->address,
                ':c' => $this->designations,
                ':d' => $this->email,
                ':de' => $this->department,
                ':cnt' => $this->contact,
                ':cre' => $this->credit,
                ':id' => $this->id
            );


            $status = $stmt->execute($data);

            if($status){
//     echo "kaj hoisa mena hoy";
//                $_SESSION['Message']="<h>success </h1>";
//                echo "$_SESSION[Message]";
             header('location:show_all_teachers.php');

            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }


    public function delete($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "DELETE FROM teachers WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        header('location:show_all_teachers.php');
    }



    public function getteachers(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select * from teachers";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }

    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO teachers (id,name,address,email,contact,designations,credit,department)  VALUES (:id,:i,:u,:c,:d,:dd,:s,:ss)";


            $stmt = $pdo->prepare($query);

             print_r($stmt);

            $data = array(
                ':id' => null,
                ':i' => $this->name,
                ':u' => $this->address,
                ':c' => $this->email,
                ':d' => $this->contact,
                ':dd' => $this->designations,
                ':s'=> $this->credit,

                ':ss' => $this->department,
                //':e' => $this->email

            );
            print_r($data);
            $status=   $stmt->execute($data);
            // print_r($status);
            if($status){

                $_SESSION['Message']="<h>success </h1>";
                echo "$_SESSION[Message]";
                header('location:save_teachers.php');

            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }
}
