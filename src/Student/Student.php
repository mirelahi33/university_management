<?php

/**
 * Created by PhpStorm.
 * User: Ayon
 * Date: 17-Apr-17
 * Time: 2:25 PM
 */
namespace  App\Student;
use PDO;
//session_start();
class Student
{   private $id;
    private $email;
    private $name;
    private $address;
    private $department;
    private $date;
    private $dbuser = 'root';
    private $dbpass = '';
    private $regnumber;


    public function setData($data = '')
    {
        if(!empty($data['name'])) {
         $this->name = $data['name'];
        }
       if(!empty($data['address'])) {
           $this->address = $data['address'];
       }
       if(!empty($data['department'])){
        $this->department=$data['department'];
       }
       if(!empty($data['email'])){
        $this->email = $data['email'];
       }
       if(!empty($data['date'])) {
           $this->date = $data['date'];
       }
       if(!empty($data['id'])){
           $this->id=$data['id'];
       }
        return $this;
    }

    public function getstudents(){

        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "SELECT * FROM students ";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }



    public function showData($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "SELECT * FROM students WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }


    public function upDate(){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "UPDATE students SET name=:i,email=:d,createdAt=:dt,address=:u,department=:de WHERE id=:id";
            $stmt = $pdo->prepare($query);
            $data = array(
                ':i' => $this->name,
                ':d' => $this->email,
                ':dt'=>date('Y-m-d h:m:s'),
                ':u' => $this->address,
                ':de' => $this->department,
                ':id' => $this->id
            );


            $status = $stmt->execute($data);

            if($status){
//       echo "kaj hoisa mena hoy";
//                $_SESSION['Message']="<h>success </h1>";
//                echo "$_SESSION[Message]";
           header('location:show_register_student.php');
//
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "DELETE FROM students WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        header('location:show_register_student.php');
    }



    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO students (id,name,email,address,department,createdAt,regnumber)  VALUES (:id,:i,:u,:c,:d,:dd,:rn)";

            $stmt = $pdo->prepare($query);

            if(!empty($this->department)) {

                $numRows = $pdo->query("select * from students
                where department='$this->department'")->rowCount();
                $nRows = $numRows + 1;
                if ($nRows < 10) {
                    $this->regnumber = $this->department . "-" . date('Y') . "-" . "00" . $nRows;
                }
                else if ($nRows >= 10 && $nRows <= 99) {
                    $this->regnumber = $this->department . "-" . date('Y') . "-" . "0" . $nRows;
                }
                else if ($nRows >= 100) {
                    $this->regnumber = $this->department . "-" . date('Y') . "-" . $nRows;
                }
            }


            $data = array(
                ':id'=>null,
                ':i' => $this->name,
                ':u' => $this->email,
                ':c' => $this->address,
                ':d' => $this->department,
//                ':dd'=> date($this->date)
                ':dd'=> date('Y-m-d h:m:s'),
                ':rn' => $this->regnumber

            );
            $status=   $stmt->execute($data);
            if($status){

                $_SESSION['Message']="<h>success </h1>";
                echo "$_SESSION[Message]";
                header('location:register_student.php');

            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }


    }
}
