<?php

namespace App\Allocate_classroom;
use PDO;

class Allocate_classroom
{
    private $course;
    private $department;
    private $room;
    private $day;
    private $from;
    private $to;
    //  private $name;
    // private $credit;
    private $dbuser = 'root';
    private $dbpass = '';

    public function setData($data = '')
    {
        $this->course = $data['course_name'];
        $this->department = $data['department_name'];
        $this->room = $data['room_no'];
        $this->day = $data['day'];
        $this->from = $data['time1'];
        $this->to = $data['time2'];
        //   $this->credit = $data['credit'];

        return $this;

    }

    public function time(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select from_time,to_time from allocate_room";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }

    public function room(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select room_no from allocate_room";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }

    public function day(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select day from allocate_room";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }


    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO allocate_room(department,course,room_no,day,from_time,to_time) VALUES (:i,:u,:id,:ud,:ss,:aa)";
            $stmt = $pdo->prepare($query);

            $data = array(

                ':i' => $this->department,
                ':u' => $this->course,
                ':id' => $this->room,
                ':ud' => $this->day,
                ':ss' => $this->from,
                ':aa' => $this->to
            );
                $status=   $stmt->execute($data);


            if($status){


//                $_SESSION['Message']="<h>success </h1>";
//                echo "$_SESSION[Message]";
                header('location:allocate_classroom.php');

            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }
}