<?php

namespace App\Student_Result;
use PDO;
@session_start();
class Student_Result
{
    private $id;
    private $student_reg_no;
    private $name;
    private $email;
    private $department;
    private $select_course;
    private $letter;
    private $dbuser = 'root';
    private $dbpass = '';

    public function setData($data = '')
    {
        if(!empty($_POST['student_reg_no'])){
            $this->student_reg_no = $data['student_reg_no'];
        }
        if(!empty($_POST['name'])){
            $this->name = $data['name'];
        }
        if(!empty($_POST['email'])){
            $this->email = $data['email'];
        }
        if(!empty($_POST['department'])){
            $this->department = $data['department'];
        }
        if(!empty($_POST['select_course'])){
            $this->select_course = $data['select_course'];
        }
        if(!empty($_POST['letter'])){
            $this->letter = $data['letter'];
        }

//      echo "<pre>";
//        print_r($this);
//        die();

    }

    public function getStdResult(){

        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select * from  student_result";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO student_result (id,student_reg_no,name,email,department,select_course,letter)  VALUES (:id,:i,:u,:c,:d,:dd,:s)";
            $stmt = $pdo->prepare($query);

            $data = array(
                ':id' => null,
                ':i' => $this->student_reg_no,
                ':u' => $this->name,
                ':c' => $this->email,
                ':d' => $this->department,
                ':dd' => $this->select_course,
                ':s'=> $this->letter
            );
            $status=   $stmt->execute($data);

            if($status) {
                $_SESSION['Msg'] = "Data Insert Successfully";
                header('location:save_student_result.php');
            }
//            else{
//               // $_SESSION['Msg'] = "Data Insertion Failed.Insert a Value that does not exist.";
//                header('location:student_result.php');
//            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }

}