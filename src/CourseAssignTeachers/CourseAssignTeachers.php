<?php

namespace App\CourseAssignTeachers;
use PDO;

class CourseAssignTeachers
{
    private $id;
    private $department;
    private $teacher;
    private $credit_taken;
    private $remaining_credit;
    private $course_code;
    private $name;
    private $credit;
    private $dbuser = 'root';
    private $dbpass = '';

    public function setData($data = '')
    {
        $this->department = $data['department_name'];
        $this->teacher = $data['teacher_name'];
        $this->credit_taken = $data['credit_taken'];
        $this->remaining_credit = $data['remaining_credit'];
        $this->course_code = $data['course_code'];
        $this->name = $data['name'];
        $this->credit = $data['credit'];

        return $this;

    }

    public function getcourseteachers(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select * from  course_assign_teachers";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }


    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO course_assign_teachers (id,department,teacher,credit_taken,remaining_credit,course_code,name,credit)  VALUES (:id,:i,:u,:c,:d,:dd,:s,:cdt)";
            $stmt = $pdo->prepare($query);

            $data = array(
                ':id' => null,
                ':i' => $this->department,
                ':u' => $this->teacher,
                ':c' => $this->credit_taken,
                ':d' => $this->remaining_credit,
                ':dd' => $this->course_code,
                ':s'=> $this->name,
                'cdt'=> $this->credit
            );
            $status=   $stmt->execute($data);

            if($status){


//                $_SESSION['Message']="<h>success </h1>";
//                echo "$_SESSION[Message]";
               header('location:course_assign_to_teacher.php');

            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }
}