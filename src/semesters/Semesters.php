<?php

/**
 * Created by PhpStorm.
 * User: Ayon
 * Date: 17-Apr-17
 * Time: 2:25 PM
 */
namespace  App\semesters;
use PDO;
//session_start();
class Semesters
{
    private $code;
    private $name;
    private $dbuser = 'root';
    private $dbpass = '';


    public function setData($data = '')
    {

        $this->code = $data['code'];
        $this->name= $data['name'];
        return $this;
    }


    public function getsemesters(){

        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "SELECT semester FROM semesters ";

        $stmt = $pdo->prepare($query);

        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }


    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO departments (code,name)  VALUES (:i,:u)";

            $stmt = $pdo->prepare($query);

            $data = array(
                ':i' => $this->code,
                ':u' => $this->name,
            );
            $status=   $stmt->execute($data);
            if($status){

                $_SESSION['Message']="<h>success </h1>";
                echo "$_SESSION[Message]";
                header('location:../../index.php');

            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }
}
