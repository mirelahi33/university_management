<?php

namespace  App\courses;
use PDO;
//session_start();
class Save_courses
{   private $id;
    private $code;
    private $name;
    private $description;
    private $credit;
    private $department;
    private $semester;
    private $dbuser = 'root';
    private $dbpass = '';

    public function setData($data = '')
    {
        if(!empty($data['code'])){
            $this->code = $data['code'];
        }
        if(!empty($data['name'])){
            $this->name = $data['name'];
        }
        if(!empty($data['credit'])){
            $this->credit = $data['credit'];
        }
        if(!empty($data['description'])){
            $this->description = $data['description'];
        }
        if(!empty($data['department'])){
            $this->department = $data['department'];
        }
        if(!empty($data['semester'])){
            $this->semester = $data['semester'];
        }
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        return $this;

    }


    public function showData($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "SELECT * FROM courses WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public function upDate(){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "UPDATE courses SET code=:i,name=:u,credit=:c,description=:d,department=:de,semester=:se WHERE id=:id";

            $stmt = $pdo->prepare($query);

            $data = array(
                ':i' => $this->code,
                ':u' => $this->name,
                ':c' => $this->credit,
                ':d' => $this->description,
                ':de' => $this->department,
                ':se' => $this->semester,
                ':id' => $this->id
            );


            $status = $stmt->execute($data);

            if($status){

//                $_SESSION['Message']="<h>success </h1>";
//                echo "$_SESSION[Message]";
       header('location:show_all_course.php');

            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "DELETE FROM courses WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        header('location:show_all_course.php');
    }




    public function getcourse(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select * from courses";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }
    public function getrooms(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select * from rooms";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }
    public function getdays(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "select * from days";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }


    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO courses (id,code,name,credit,description,department,semester)  VALUES (:id,:i,:u,:c,:d,:dd,:s)";
            $stmt = $pdo->prepare($query);

            $data = array(
                ':id' => null,
                ':i' => $this->code,
                ':u' => $this->name,
                ':c' => $this->credit,
                ':d' => $this->description,
                ':dd' => $this->department,
                ':s'=> $this->semester
            );
            $status=   $stmt->execute($data);
            if($status){

                $_SESSION['Message']="<h>success </h1>";
                echo "$_SESSION[Message]";
               header('location:save_course.php');

            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }
}
