<?php

namespace  App\save_department;
use PDO;
@session_start();
class Save_department
{
    private $id;
    private $code;
    private $name;
    private $dbuser = 'root';
    private $dbpass = '';



    public function setData($data = '')
    {
     if($_POST['code']=='0') {
         header('location:save_department.php');
          $_SESSION['Message']="<h>Oi 0 disos keno?? </h1>";
     }
     else {

         if (!empty($_POST['code'])) {
             $this->code = $_POST['code'];
         }

     }

        if(!empty($_POST['name'])){
            $this->name = $_POST['name'];
        }
        if(!empty($_POST['id'])){
            $this->id = $_POST['id'];
        }

        return $this;

    }


    public function getdepartment(){

        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);

        $query = "SELECT id,code,name FROM departments ";

        $stmt = $pdo->prepare($query);

        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
        //  echo"<pre>";
        //  print_r($data);

    }

    public function showData($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);

        $query = "SELECT * FROM departments WHERE id=$id";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;


    }

    public function update(){
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "UPDATE departments SET code=:i,name=:u WHERE id=:id";

            $stmt = $pdo->prepare($query);

            $data = array(
                ':i' => $this->code,
                ':u' => $this->name,
                ':id' =>$this->id,
            );
       $status = $stmt->execute($data);

            if($status){
               // $_SESSION['Msg'] = "Data Update Successfully";
                header('location:view_all_department.php');
            }
          } catch (PDOException $e) {
              echo 'Error: ' . $e->getMessage();
          }
    }


    public function delete($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "DELETE FROM departments WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $status = $stmt->execute();

        if($status){
            $_SESSION['Msg'] = "Data Delete Successfully";
        header('location:view_all_department.php');
        }
    }



    public function getdesignation(){


        $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
        $query = "SELECT designation FROM designations ";
        $stmt = $pdo->prepare($query);

        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
        //  echo"<pre>";
        //  print_r($data);

    }

    public function store()
    {
        try {

            $pdo = new PDO('mysql:host=localhost;dbname=result_process_system', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO departments (id,code,name)  VALUES (:id,:i,:u)";


            $stmt = $pdo->prepare($query);

            $data = array(
                ':id' => null,
                ':i' => $this->code,
                ':u' => $this->name

            );
            $status=   $stmt->execute($data);
            if($status) {
                $_SESSION['Msg'] = "Data Insert Successfully";
                header('location:save_department.php');
            }
            else{
                $_SESSION['Msg'] = "Data Insertion Failed.Insert a Value that does not exist.";
                header('location:save_department.php');
            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();


        }


    }
}
