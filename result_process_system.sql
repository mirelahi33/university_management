-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2017 at 05:10 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `result_process_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `allocate_room`
--

CREATE TABLE `allocate_room` (
  `id` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `room_no` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allocate_room`
--

INSERT INTO `allocate_room` (`id`, `department`, `course`, `room_no`, `day`, `from_time`, `to_time`) VALUES
(6, 'EEE', 'DMATH', '301', 'Saturday', '00:01:00', '02:00:00'),
(8, 'CSE', 'C Programming Langua', '302', 'Saturday', '04:00:00', '05:01:00'),
(9, 'CSE', 'C Programming Langua', '302', 'Saturday', '04:00:00', '05:01:00'),
(10, 'CSE', 'C Programming Langua', '305', 'Saturday', '01:00:00', '02:00:00'),
(11, 'LLB', 'DMATH', '303', 'Friday', '03:00:00', '04:00:00'),
(12, 'CSE', 'ECIRCUIt', '303', 'Sunday', '03:00:00', '04:00:00'),
(13, 'CSE', 'ECIRCUIt', '303', 'Sunday', '15:00:00', '16:00:00'),
(14, 'CSE', 'ECIRCUIt', '303', 'Sunday', '13:00:00', '14:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `credit` double NOT NULL,
  `description` varchar(256) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL,
  `semester` varchar(250) NOT NULL,
  `teachers_id` int(50) NOT NULL,
  `department` varchar(250) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `code`, `name`, `credit`, `description`, `createdAt`, `updatedAt`, `deletedAt`, `semester`, `teachers_id`, `department`, `department_id`) VALUES
(41, 'DMATH-123', 'DMATH', 3, 'Something........', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1.2', 13, 'CSE', 0),
(42, 'PL-134', 'C Programming Langua', 3, 'Basic Programming language', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2.1', 14, 'CSE', 0),
(43, 'EC-423', 'ECIRCUIt', 4, 'nothing...', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1.2', 13, 'EEE', 0),
(44, 'EL-323', 'English 1', 4, 'Valo lagena....', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2.1', 15, 'LLB', 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_assign_teachers`
--

CREATE TABLE `course_assign_teachers` (
  `id` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `teacher` varchar(255) NOT NULL,
  `credit_taken` int(11) NOT NULL,
  `remaining_credit` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `credit` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_assign_teachers`
--

INSERT INTO `course_assign_teachers` (`id`, `department`, `teacher`, `credit_taken`, `remaining_credit`, `course_code`, `name`, `credit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CSE', '14', 12, 9, '41', 'DMATH', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'EEE', '16', 20, 20, '43', 'ECIRCUIt', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'LLB', '17', 15, 15, '44', 'English 1', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'EEE', '15', 14, 14, '43', 'ECIRCUIt', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'CSE', '13', 20, 11, '42', 'C Programming Langua', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'CSE', '13', 20, 11, '42', 'C Programming Langua', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'CSE', '13', 20, 11, '42', 'C Programming Langua', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_teacher_mapping`
--

CREATE TABLE `course_teacher_mapping` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_credit` float NOT NULL,
  `department_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_teacher_mapping`
--

INSERT INTO `course_teacher_mapping` (`id`, `department_id`, `teacher_id`, `course_code`, `course_credit`, `department_name`) VALUES
(1, 78, 13, 'DMATH-123', 4, '0'),
(2, 79, 14, 'EC-423', 3, '0'),
(3, 81, 13, 'PL-134', 5, '0');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `day`) VALUES
(10, 'Saturday'),
(11, 'Sunday'),
(12, 'Monday'),
(13, 'Tuesday'),
(14, 'Wednesday'),
(15, 'Thursday'),
(16, 'Friday');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `code`, `name`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(78, '001', 'CSE', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, '002', 'EEE', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, '003', 'LLB', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, '005', 'BBA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `designation` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation`) VALUES
(1, 'Lecturer'),
(2, 'Professor'),
(3, 'Assistant Professor');

-- --------------------------------------------------------

--
-- Table structure for table `enroll_course`
--

CREATE TABLE `enroll_course` (
  `id` int(20) NOT NULL,
  `student_reg_no` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `select_course` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll_course`
--

INSERT INTO `enroll_course` (`id`, `student_reg_no`, `name`, `email`, `department`, `select_course`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', 'DMATH', '2017-05-08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'EEE-2017-003', 'Nur', 'nur@gmail.com', 'EEE', 'C Programming Langua', '2017-05-08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'EEE-2017-005', 'Anowar', 'anowar@gmail.com', 'EEE', 'English 1', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'BBA-2017-002', 'dfaf', 'dfa@gmail.com', 'BBA', 'English 1', '2017-05-08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'CSE-2017-001', 'jafa', 'jara@gmil.com', 'CSE', 'PHP', '2017-05-07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', 'DMATH', '2017-05-08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'EEE-2017-005', 'Anowar', 'anowar@gmail.com', 'EEE', 'English 1', '2017-05-10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', 'ECIRCUIt', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', 'ECIRCUIt', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'BBA-2017-002', 'dfaf', 'dfa@gmail.com', 'BBA', 'C Programming Langua', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'BBA-2017-002', 'dfaf', 'dfa@gmail.com', 'BBA', 'OOP PHP7', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'CSE-2017-001', 'jafa', 'jara@gmil.com', 'CSE', 'PHP', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'CSE-2017-001', 'jafa', 'jara@gmil.com', 'CSE', 'OOP PHP7', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'EEE-2017-004', 'Mohammad', 'mohammad@gmail.com', 'EEE', 'ECIRCUIt', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'EEE-2017-002', 'Nahid', 'nmnahid@gmail.com', 'EEE', 'C Programming Langua', '2017-05-02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'EEE-2017-002', 'Nahid', 'nmnahid@gmail.com', 'EEE', 'PHP', '2017-05-09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `letter` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `letter`) VALUES
(2, 'A+'),
(3, 'A'),
(4, 'A-'),
(5, 'B+'),
(6, 'B'),
(7, 'B-'),
(8, 'C+'),
(9, 'C'),
(10, 'C-'),
(11, 'D+'),
(12, 'D'),
(13, 'D-'),
(14, 'F');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_no`) VALUES
(1, 301),
(2, 302),
(3, 303),
(4, 304),
(5, 305);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `days_id` int(11) NOT NULL,
  `times_id` int(11) NOT NULL,
  `rooms_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `semester` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `semester`) VALUES
(1, '1.1'),
(2, '1.2'),
(3, '2.1'),
(4, '2.2'),
(5, '3.1'),
(6, '3.2'),
(7, '4.1'),
(8, '4.2');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(256) NOT NULL,
  `department` varchar(255) NOT NULL,
  `regnumber` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `email`, `address`, `department`, `regnumber`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(3, 'jahid', 'jahid@gmail.com', 'Dhaka', 'EEE', 'EEE-2017-001', '2017-05-04 11:05:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Nahid', 'nmnahid@gmail.com', 'Feni', 'EEE', 'EEE-2017-002', '2017-05-04 11:05:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Nur', 'nur@gmail.com', 'Kawran bazar, Dhaka', 'EEE', 'EEE-2017-003', '2017-05-04 11:05:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Mohammad', 'mohammad@gmail.com', 'Feni', 'EEE', 'EEE-2017-004', '2017-05-04 11:05:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Anowar', 'anowar@gmail.com', 'Feni', 'EEE', 'EEE-2017-005', '2017-05-04 11:05:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Ayon', 'ayon@gmail.com', 'Dhaka', 'EEE', 'EEE-2017-006', '2017-05-04 11:05:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Anik', 'anik@gmail.com', 'Dhaka', 'EEE', 'EEE-2017-007', '2017-05-04 11:05:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Prince', 'prince@gmail.com', 'Dhaka', 'EEE', 'EEE-2017-008', '2017-05-04 11:05:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'jafa', 'jara@gmil.com', 'Dhaka', 'CSE', 'CSE-2017-001', '2017-05-04 11:05:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'nai', 'nai@gmail.com', 'janina', 'BBA', 'BBA-2017-001', '2017-05-04 12:05:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'fjaljfl', 'dfjal@gmail.com', 'dfah', 'CSE', 'CSE-2017-002', '2017-05-04 12:05:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'dfaf', 'dfa@gmail.com', 'dsfsaf', 'BBA', 'BBA-2017-002', '2017-05-04 12:05:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Abul Mal', 'abdul@mudhit.com', 'janena', 'CSE', 'CSE-2017-003', '2017-05-04 01:05:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_result`
--

CREATE TABLE `student_result` (
  `id` int(20) NOT NULL,
  `student_reg_no` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `select_course` varchar(255) NOT NULL,
  `letter` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_result`
--

INSERT INTO `student_result` (`id`, `student_reg_no`, `name`, `email`, `department`, `select_course`, `letter`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', '1', 'A', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', '1', 'A+', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', '8', 'A+', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', '8', 'A+', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'EEE-2017-001', 'jahid', 'jahid@gmail.com', 'EEE', '1', 'A+', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'EEE-2017-002', 'Nahid', 'nmnahid@gmail.com', 'EEE', '16', 'A', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(256) NOT NULL,
  `email` varchar(255) NOT NULL,
  `designations` varchar(255) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `credit` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `address`, `email`, `designations`, `contact`, `password`, `credit`, `department`, `department_id`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(13, 'Ayon', 'Kolabagan,Dhaka', '', 'Professor', '01734543544', '', 20, 'CSE', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Nahid', 'Kawranbazar,Dhaka', '', 'Professor', '01431109602', '', 12, 'CSE', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'onik', 'Kolabagan,Dhaka', '', 'Professor', '01743533453', '', 20, 'EEE', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Prince', 'Dhaka ', '', 'Professor', '01232331313', '', 15, 'LLB', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Google Mahmud', 'USA', '', 'Professor', '2343242', '', 10, 'CSE', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE `times` (
  `id` int(11) NOT NULL,
  `started_from` varchar(20) NOT NULL,
  `ended_to` varchar(20) NOT NULL,
  `meridien` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `times`
--

INSERT INTO `times` (`id`, `started_from`, `ended_to`, `meridien`) VALUES
(1, '1494410653', '1494410653', ''),
(2, '1494410663', '1494410663', ''),
(3, '11:00', '13:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allocate_room`
--
ALTER TABLE `allocate_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `teacher_id` (`teachers_id`),
  ADD KEY `semesters_id` (`semester`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `teachers_id` (`teachers_id`);

--
-- Indexes for table `course_assign_teachers`
--
ALTER TABLE `course_assign_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_teacher_mapping`
--
ALTER TABLE `course_teacher_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_code` (`course_code`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `department_name` (`department_name`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enroll_course`
--
ALTER TABLE `enroll_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departments_id` (`departments_id`),
  ADD KEY `courses_id` (`courses_id`),
  ADD KEY `days_id` (`days_id`),
  ADD KEY `times_id` (`times_id`),
  ADD KEY `rooms_id` (`rooms_id`),
  ADD KEY `departments_id_2` (`departments_id`),
  ADD KEY `courses_id_2` (`courses_id`),
  ADD KEY `days_id_2` (`days_id`),
  ADD KEY `days_id_3` (`days_id`),
  ADD KEY `times_id_2` (`times_id`),
  ADD KEY `rooms_id_2` (`rooms_id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_result`
--
ALTER TABLE `student_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `department_id_2` (`department_id`),
  ADD KEY `department_id_3` (`department_id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allocate_room`
--
ALTER TABLE `allocate_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `course_assign_teachers`
--
ALTER TABLE `course_assign_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `course_teacher_mapping`
--
ALTER TABLE `course_teacher_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `enroll_course`
--
ALTER TABLE `enroll_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `student_result`
--
ALTER TABLE `student_result`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_teacher_mapping`
--
ALTER TABLE `course_teacher_mapping`
  ADD CONSTRAINT `course_teacher_mapping_ibfk_1` FOREIGN KEY (`course_code`) REFERENCES `courses` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `course_teacher_mapping_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `course_teacher_mapping_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `schedules_ibfk_2` FOREIGN KEY (`courses_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `schedules_ibfk_3` FOREIGN KEY (`rooms_id`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `schedules_ibfk_4` FOREIGN KEY (`times_id`) REFERENCES `times` (`id`),
  ADD CONSTRAINT `schedules_ibfk_5` FOREIGN KEY (`days_id`) REFERENCES `days` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
