// Wait for the DOM to be ready
$(function() {
    // Initialize form validation on the registration form.

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");
    // It has the name attribute "registration"
    $("form[name='enroll_course']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
          //  code: "required",

          /*  email: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },*/
            student_reg_no: {
                required: true
                // minlength: 5
               // maxlength:7
            },
            name:{
                required: true
            },

            select_course: {
                required: true
               /* lettersonly: true*/

            },
            date:{
                required: true
            }


        },
        // Specify validation error messages
        messages: {
            student_reg_no: "Please select reg no ",
            name:{
                required:"plz provide your name"
            },

            select_course: {
                required: "Please select course",
               // minlength: "Your code must be at least 5 characters long"
              /*  maxlength:"Your password must be max 7 characters long"*/

             date:{
                 required: "Please select course"
             }

            } //email: "Please enter a valid email address"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
            form.submit();
        }
    });
});