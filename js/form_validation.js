// Wait for the DOM to be ready
$(function() {
    // Initialize form validation on the registration form.

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");
    // It has the name attribute "registration"
    $("form[name='save_department']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
          //  code: "required",

          /*  email: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },*/
            code: {
                required: true,
                minlength: 2,
                maxlength:7
               // min: 1
            },
            name: {
                required: true

            }
        },
        // Specify validation error messages
        messages: {
            name: "Please enter your department name ",

            code: {
                required: "Please provide a code",
                minlength: "Your code must be at least 2 characters long",
                maxlength:"Your code must be max 7 characters long"
            }
            //email: "Please enter a valid email address"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
            form.submit();
        }
    });
});